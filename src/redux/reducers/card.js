import { actionType } from "../actions/type";

let initialState = {
  deckCard: {},
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_CARDS:
      state.deckCard = payload;
      return { ...state };

    default:
      return state;
  }
};

export default reducer;
