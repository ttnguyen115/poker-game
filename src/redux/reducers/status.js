import { actionType } from "../actions/type";

const initialState = {
    isShuffled: false,
    isDrew: false,
    isRevealed: false,
    gameCount: 0
}

const status = (state = initialState, { type, payload }) => {
    switch (type) {
        case actionType.SHUFFLE_CARDS:
            state.isShuffled = true;
            state.isDrew = false;
            state.isRevealed = false;
            state.gameCount++;
            return { ...state };
        
        case actionType.DRAW_CARDS:
            state.isShuffled = false;
            state.isDrew = true;
            state.isRevealed = false;
            return { ...state };
        
        case actionType.REVEAL_CARDS:
            state.isShuffled = false;
            state.isDrew = false;
            state.isRevealed = true;
            return { ...state };

        default:
            return state;
    }
}

export default status