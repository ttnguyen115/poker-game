import axios from 'axios';
import { createAction } from '.';
import { actionType } from './type';

export const fetchCards = (players, deckId) => async dispatch => {
    try {
        const res = await axios.get(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=12`);
        const playerList = [...players];
        
        for (let i in res.data.cards) {
          const playerIndex = i % playerList.length;
          playerList[playerIndex].cards.push(res.data.cards[i]);
        }
        dispatch(createAction(actionType.SET_PLAYERS, playerList))
      } catch (err) {
        console.log({ ...err });
      }
}

export const shuffleCards = deckId => async dispatch => {
    try {
        const res = await axios.get(`https://deckofcardsapi.com/api/deck/${deckId}/shuffle/`);
        dispatch(createAction(actionType.SET_CARDS, res.data));
    } catch (err) {
        console.log({ ...err })
    }
}