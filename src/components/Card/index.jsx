import React from "react";
import "./card.css";

const Card = ({ card, isRevealed }) => {
  return (
    <div>
      <img
        alt="card"
        className="card"
        src={
          isRevealed 
          ? card.images.png
          : "https://i.pinimg.com/originals/10/80/a4/1080a4bd1a33cec92019fab5efb3995d.png"
        }
      />
    </div>
  );
};

export default Card;
