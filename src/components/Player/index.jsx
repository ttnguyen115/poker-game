import React from "react";
import { useSelector } from "react-redux";
import Card from "../Card";

const Player = ({ player, index }) => {
  const isRevealed = useSelector(state => state.status.isRevealed);

  return (
    <div className={`player-${index}`}>
      <p className="lead">{player.username}</p>
      <main className="d-flex">
        {
          player.cards.map(card => (
            <Card key={card.code} card={card} isRevealed={isRevealed} />
          ))
        }
      </main>
    </div>
  );
};

export default Player;
