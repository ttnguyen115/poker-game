import axios from "axios";
import React, { useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createAction } from '../../redux/actions';
import { fetchCards, shuffleCards } from "../../redux/actions/cardAction";
import { actionType } from './../../redux/actions/type';

const Control = () => {
  const dispatch = useDispatch();
  const { isShuffled, isDrew, isRevealed } = useSelector(state => state.status);
  const players = useSelector(state => state.player.playerList);
  const deckCard = useSelector(state => state.card.deckCard);

  // click shuffle
  const handleShuffleCard = () => {
    dispatch(createAction(actionType.SHUFFLE_CARDS, false));
    dispatch(createAction(actionType.RESET_PLAYER_CARDS, {}));
    dispatch(shuffleCards(deckCard.deck_id));
  };

  // click draw
  const handleFetchCards = async () => {
    dispatch(fetchCards(players, deckCard.deck_id));
    dispatch(createAction(actionType.DRAW_CARDS, {}));
  };

  // click reveal
  const handleRevealCards = () => {
    dispatch(createAction(actionType.REVEAL_CARDS, {}));
    checkResult();
  }
  
  const checkResult = () => {
    const playerList = [...players];
    const winners = [];
    
    // update score for winners
    const updateScore = (winners) => {
      const depositScorePlayerList = playerList.map(player => ({ ...player, totalPoint: player.totalPoint - 5000 }));
      const updateScoreWinners = winners.map(winner => ({ ...winner, totalPoint: winner.totalPoint + Math.floor((20000 - winners.length * 5000) / winners.length) }));
      const newPlayerList = depositScorePlayerList.map(player => updateScoreWinners.find(winner => winner.username === player.username) || player);
      
      // dispatch store to update playerList
      dispatch(createAction(actionType.SET_PLAYERS, newPlayerList))
      return;
    }
    
    // Check special case => if yes => winners push
    playerList.map(player => checkSpecialCase(player)).map(winner => winner && winners.push(winner));
    if (winners.length !== 0) updateScore(winners);
    
    // if not => count total each player and find max
    const playerScore = playerList.map(player => countPlayerScore(player.cards))
    playerScore
      .reduce((acc, currentValue, index) => currentValue === Math.max(...playerScore) ? acc.concat(index) : acc, [])
      .map(playerIndex => winners.push(playerList[playerIndex]));
    updateScore(winners);
  };

  const checkSpecialCase = (player) => {
    let count = 0;
    player.cards.forEach(card => ((card.value === 'KING' || card.value === 'QUEEN' || card.value === 'JACK') && count++ ));
    return count === 3 ? player : null;
  }

  const countPlayerScore = (cards) => {
    return +cards
      .reduce((totalScore, currentCard) => totalScore + convertCardValue(currentCard.value), 0)
      .toString().split('').pop();
  }

  const convertCardValue = value => {
    switch (value) {
      case 'KING':
      case 'QUEEN':
      case 'JACK':
        return 10;
    
      case 'ACE': 
        return 1;
        
      default:
        return +value;
    }
  }

  return (
    <div className="container d-flex justify-content-end">
      <div className="px-2 border d-flex justify-content-center align-items-center">
        <button className="mr-2 btn btn-success" onClick={handleShuffleCard} disabled={!isRevealed}>Shuffle</button>
        <button className="mr-2 btn btn-info" onClick={handleFetchCards} disabled={!isShuffled}>Draw</button>
        <button className="mr-2 btn btn-primary" onClick={handleRevealCards} disabled={!isDrew}>Reveal</button>
      </div>
      
      <div className="d-flex">
        {
          players.map(player => (
            <div key={player.username} className="px-3 text-center border">
              <p>{player.username}</p>
              <p>{player.totalPoint}</p>
            </div>
          ))
        }
      </div>
    </div>
  );
};

export default Control;
