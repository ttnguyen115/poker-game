import axios from "axios";
import React, { Fragment, useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Controls from "../../components/Control";
import Main from "../../components/Main";
import { createAction } from '../../redux/actions';
import { actionType } from '../../redux/actions/type';
import "./index.css";

const Game = () => {
  const dispatch = useDispatch();
  const { gameCount } = useSelector(state => state.status);
  const { playerList } = useSelector(state => state.player);

  const fetchNewDeck = useCallback(async () => {
    try {
      const res = await axios.get('https://deckofcardsapi.com/api/deck/new/');
      dispatch(createAction(actionType.SET_CARDS, res.data));
      localStorage.setItem('deck_id', res.data.deck_id);
    } catch (err) {
      console.log({ ...err })
    }
  }, [dispatch]);
  
  const shuffleDeck = useCallback(async deckId => {
    try {
      const res = await axios.get(`https://deckofcardsapi.com/api/deck/${deckId}/shuffle/`);
      dispatch(createAction(actionType.SET_CARDS, res.data));
      dispatch(createAction(actionType.SHUFFLE_CARDS, true));
    } catch (err) {
      console.log({ ...err })
    }
  }, [dispatch]);

  useEffect(() => {
    const deckId = localStorage.getItem('deck_id');
    if (deckId) shuffleDeck(deckId);
    else fetchNewDeck();
  }, [fetchNewDeck, shuffleDeck]);

  useEffect(() => {
    if (gameCount === 6) {
      const maxPoint = Math.max(...playerList.map(player => player.totalPoint));
      const winners = playerList.filter(player => player.totalPoint === maxPoint);
      alert(`
        Winner(s): 
        ${winners.map(winner => `${winner.username}`)}
      `);
    }
  }, [gameCount, playerList]);

  return (
    <Fragment>
      <Controls />
      <Main />
    </Fragment>
  );
}

export default Game;
