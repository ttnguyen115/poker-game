import { useFormik } from "formik";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import * as yup from 'yup';
import { actionType } from "../../redux/actions/type";
import Game from "../Game";
import { createAction } from './../../redux/actions/index';

const schema = yup.object().shape({
  username: yup.string().required('Username is required!'),
  email: yup.string().required('Email is required!').email('Email is invalid!'),
  phone: yup.string().required('Phone is required!').matches(/^[0-9]+$/g),
})

const Home = () => {
  const dispatch = useDispatch();
  const [isGameStarted, setIsGameStarted] = useState(false);
  
  const { handleChange, values, errors, touched, handleBlur, setTouched, isValid } = useFormik({
    initialValues: {
      username: 'Nguyễn Thành Trung',
      email: '123@123.com',
      phone: '123123123'
    },

    validationSchema: schema,
    validateOnMount: true,
  });

  const handleSubmit = e => {
    e.preventDefault();
    setTouched({
      username: true,
      email: true,
      phone: true,
    })

    if (!isValid) return;
    else console.log(values);
    dispatch(createAction(actionType.ADD_PLAYERS, { ...values, totalPoint: 25000, cards: [] }));
    setIsGameStarted(true);
  }

  return (
    <>
      {
        isGameStarted 
        ? <Game />
        : <div
          className="text-center"
          style={{
            width: "100vw",
            height: "100vh",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <h1 className="mb-5 diplay-4"> Welcome to Pocker Center</h1>
          <h3>Fill your info and start</h3>
          <form className="mx-auto w-25" onSubmit={handleSubmit}>
            <input
              name="username"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.username}
              type="input"
              placeholder="username"
              className="mb-3 w-100 form-control"
            />
            { touched.username && <p className="text-danger">{errors.username}</p> }

            <input
              name="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              type="input"
              placeholder="email"
              className="mb-3 w-100 form-control"
            />
            { touched.email && <p className="text-danger">{errors.email}</p> }

            <input
              name="phone"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.phone}
              type="input"
              placeholder="phone"
              className="mb-3 w-100 form-control"
            />
            { touched.phone && <p className="text-danger">{errors.phone}</p> }

            <button className="btn btn-success">Start new Game</button>
          </form>
        </div>
      }
    </>
  );
};

export default Home;
